#!/bin/sh

docker stop $(docker ps | grep 'lgk8s')
docker rm -fv $(docker ps -a -q)
docker rmi $(docker images -a | grep '<none>')
docker rmi $(docker images -a | grep 'lgk8s')