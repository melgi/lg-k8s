package be.continuum.lgk8s.web;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/payment")
@Slf4j
public class Resource {

    @GetMapping("/post/{uuid}")
    public ResponseEntity<String> processPayment(@PathVariable("uuid") final String uuid) {
        log.info("Processing (post) payment with UUID [ {} ] ", uuid);

        if(StringUtils.startsWithAny(uuid, "0123456789")) {
            throw new RuntimeException("NOT OK");
        }
        return ResponseEntity.ok("PAYMENT OK :-)");
    }
}