package be.continuum.lgk8s;

import be.continuum.lgk8s.config.SpringConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@Import(SpringConfiguration.class)
@Slf4j
public class MicroServiceB {

    public static void main(String[] args) {
        new SpringApplication(MicroServiceB.class).run(args);
    }

    @Scheduled(initialDelay = 60000L, fixedDelay = 60000L)
    void heartBeat() {
        log.info("MicroService is alive ! Next event in 60s ...");
    }
}