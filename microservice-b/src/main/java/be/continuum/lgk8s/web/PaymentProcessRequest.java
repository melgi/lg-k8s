package be.continuum.lgk8s.web;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentProcessRequest implements Serializable {

    private UUID uuid;
    private String bankAccountNumber;
    private double amount;
}