package be.continuum.lgk8s.web;

import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.UUID;

import static org.springframework.http.HttpMethod.POST;

@RestController
@RequestMapping("/api/payment")
@Slf4j
public class Resource {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${microservice.b.url}")
    private String url;

    @PostMapping("/initiate")
    public ResponseEntity<String> initiatePayment(@RequestBody PaymentRequest request) {
        Assert.isTrue(request.getAmount() > 0, "Amount > 0");
        Assert.notNull(request.getBankAccountNumber(), "Account number is required.");

        log.info("Initiating (pre) payment with amount [ {} ] on account [ {} ] ...", request.getAmount(), request.getBankAccountNumber());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        String body = new JSONObject()
                .put("uuid", UUID.randomUUID())
                .put("amount", request.getAmount())
                .put("bankAccountNumber", request.getBankAccountNumber())
                .toString();

        return restTemplate.exchange(URI.create(url), POST, new HttpEntity<>(body, headers), String.class);
    }
}