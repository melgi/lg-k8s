package be.continuum.lgk8s.web;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentRequest implements Serializable {

    private String bankAccountNumber;
    private double amount;
}