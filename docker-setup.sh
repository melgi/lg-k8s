#!/bin/sh

echo "Setting up Docker network."

echo "Creating network ..."
docker network create lgk8s --driver bridge --label type=database --subnet 10.0.69.0/28 --gateway=10.0.69.1

echo "Creating containers ..."
docker run -d \
    -p 3307:3306 -p 33070:33060 \
    --hostname lgk8s-database.com \
    --name lgk8s-database \
    --network lgk8s \
    --ip 10.0.69.4 \
    --label type=database \
    --restart no \
    lgk8s/database:1.0

docker run -d \
    -p 8080:8080 -p 5005:5005 \
    --add-host lgk8s-database.com:10.0.69.4 \
    --add-host lgk8s-database.com:10.0.69.6 \
    --hostname lgk8s-microservice-a.com \
    --name lgk8s-microservice-a \
    --network lgk8s \
    --ip 10.0.69.5 \
    --label type=app \
    --restart always \
    lgk8s/microservice-a:1.0

docker run -d \
    -p 8081:8080 -p 5006:5005 \
    --add-host lgk8s-database.com:10.0.69.4 \
    --add-host lgk8s-database.com:10.0.69.7 \
    --hostname lgk8s-microservice-c.com \
    --name lgk8s-microservice-b \
    --network lgk8s \
    --ip 10.0.69.6 \
    --label type=app \
    --restart always \
    lgk8s/microservice-b:1.0

docker run -d \
    -p 8082:8080 -p 5007:5005 \
    --add-host lgk8s-database.com:10.0.69.4 \
    --hostname lgk8s-microservice-c.com \
    --name lgk8s-microservice-c \
    --network lgk8s \
    --ip 10.0.69.7 \
    --label type=app \
    --restart always \
    lgk8s/microservice-c:1.0

docker network inspect lgk8s